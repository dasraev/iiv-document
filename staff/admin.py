from django.contrib import admin
from .models import Region, Staff, Certificate, Rank, Unit


admin.site.register(Region)
admin.site.register(Staff)
admin.site.register(Certificate)
admin.site.register(Rank)
admin.site.register(Unit)

