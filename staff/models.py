from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class Region(models.Model):
    Choice = (
        ("1", "Toshkent shahar"),
        ("2", "Toshkent viloyati"),
        ("3", "Sirdaryo viloyati"),
        ("4", "Jizzax viloyati"),
        ("5", "Samarqand viloyati"),
        ("6", "Qashqadaryo viloyati"),
        ("7", "Surxondaryo viloyati"),
        ("8", "Buxoro viloyati"),
        ("9", "Navoiy viloyati"),
        ("10", "Xorazm viloyati"),
        ("11", "Qoraqalpoqiston Respublikasi"),
        ("12", "Andijon viloyati"),
        ("13", "Namangan viloyati"),
        ("14", "Farg'ona viloyati"),
    )
    region = models.CharField(max_length=225)
    # status = models.BooleanField(default=True)
    # parent = models.CharField(max_length=2,choices=Choice,null=True)
    # parent = models.ForeignKey('self', blank=True, null=True, related_name='childs', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Xudud'
        verbose_name_plural = 'Xududlar'

    def __str__(self):
        return self.region


class Rank(BaseModel):
    rank = models.CharField(max_length=455)

    class Meta:
        verbose_name = 'Lavozim'
        verbose_name_plural = 'Lavozimlar'

    def __str__(self):
        return self.rank


class Staff(BaseModel):

    STATUS = (
        ('not_iiv', "Hali IIV  uchun ko'rib chiqilmagan"),
        ('iiv', 'IIV'),
        ("delete_iiv", 'IIVdan chiqarilgan'),
    )

    first_name = models.CharField(max_length=125)
    last_name = models.CharField(max_length=125)
    father_name = models.CharField(max_length=125)
    birth_date = models.DateField()
    image = models.ImageField(upload_to='images',blank=True,null=True)
    status = models.CharField(max_length=40, choices=STATUS)
    rank = models.ForeignKey(Rank, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Xodim'
        verbose_name_plural = 'Xodimlar'

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Unit(BaseModel):
    title = models.CharField(max_length=455)

    class Meta:
        verbose_name = "Tarkibiy Tuzilmasi"
        verbose_name_plural = 'Tarkibiy Tuzilmasi'

    def __str__(self):
        return self.title

#####

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_even(value):
    if value % 2 != 0:
        raise ValidationError(
            _('%(value)s is not an even number'),
            params={'value': value},
        )

    ####

class Certificate(BaseModel):
    staff = models.ForeignKey(Staff, related_name='staff', on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    given_region = models.ForeignKey(Region, on_delete=models.CASCADE)
    identification_num = models.CharField(max_length=20)
    serial_number = models.CharField(max_length=20)
    given_date = models.DateField()
    limit_date = models.DateField()

    class Meta:
        verbose_name = "Guvohnoma"
        verbose_name_plural = 'Guvohnomalar'

    def __str__(self):
        return '{} {}'.format(self.staff.first_name, self.staff.last_name)



