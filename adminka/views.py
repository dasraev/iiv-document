from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db.models import Q
from django.core.paginator import Paginator
from .forms import UnitForm, RankForm, RegionForm, StaffForm, CertificateForm
from django.contrib import messages
from django.contrib.auth import authenticate, login
from staff.models import Region, Rank, Staff, Certificate, Unit
from django.contrib.auth.decorators import login_required
from datetime import datetime
import datetime


def login_page(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        print("user", user)
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.info(request, 'username or password is incorrect')
    context = {}
    return render(request, 'login.html', context)


@login_required
def end_certif(request):
    if request.user.admin_user.status == '1':
        region = Region.objects.filter(Q(region='Toshkent'))
    elif request.user.admin_user.status == "2":
        region = Region.objects.filter(Q(region='Toshkent viloyati'))
    elif request.user.admin_user.status == "3":
        region = Region.objects.filter(Q(region='Sirdaryo'))
    elif request.user.admin_user.status == "4":
        region = Region.objects.filter(Q(region='Jizzax'))
    elif request.user.admin_user.status == "5":
        region = Region.objects.filter(Q(region='Samarqand'))
    elif request.user.admin_user.status == "6":
        region = Region.objects.filter(Q(region='Qashqadaryo'))
    elif request.user.admin_user.status == "7":
        region = Region.objects.filter(Q(region='Surxondaryo'))
    elif request.user.admin_user.status == "8":
        region = Region.objects.filter(Q(region='Buxoro'))
    elif request.user.admin_user.status == "9":
        region = Region.objects.filter(Q(region='Navoiy'))
    elif request.user.admin_user.status == "10":
        region = Region.objects.filter(Q(region='Xorazm'))
    elif request.user.admin_user.status == "11":
        region = Region.objects.filter(Q(region='Qoraqalpoqiston'))
    elif request.user.admin_user.status == '12':
        region = Region.objects.filter(Q(region='Andijon'))
    elif request.user.admin_user.status == "13":
        region = Region.objects.filter(Q(region='Namangan'))
    elif request.user.admin_user.status == "14":
        region = Region.objects.filter(Q(region="Farg'ona"))

    now = datetime.datetime.now()
    certifs = Certificate.objects.filter(given_region__in=region, limit_date__lte=now)

    return render(request, 'end_certif.html', {"certifs": certifs})


@login_required
def month_certif(request):
    if request.user.admin_user.status == '1':
        region = Region.objects.filter(Q(region='Toshkent'))
    elif request.user.admin_user.status == "2":
        region = Region.objects.filter(Q(region='Toshkent viloyati'))
    elif request.user.admin_user.status == "3":
        region = Region.objects.filter(Q(region='Sirdaryo'))
    elif request.user.admin_user.status == "4":
        region = Region.objects.filter(Q(region='Jizzax'))
    elif request.user.admin_user.status == "5":
        region = Region.objects.filter(Q(region='Samarqand'))
    elif request.user.admin_user.status == "6":
        region = Region.objects.filter(Q(region='Qashqadaryo'))
    elif request.user.admin_user.status == "7":
        region = Region.objects.filter(Q(region='Surxondaryo'))
    elif request.user.admin_user.status == "8":
        region = Region.objects.filter(Q(region='Buxoro'))
    elif request.user.admin_user.status == "9":
        region = Region.objects.filter(Q(region='Navoiy'))
    elif request.user.admin_user.status == "10":
        region = Region.objects.filter(Q(region='Xorazm'))
    elif request.user.admin_user.status == "11":
        region = Region.objects.filter(Q(region='Qoraqalpoqiston'))
    elif request.user.admin_user.status == '12':
        region = Region.objects.filter(Q(region='Andijon'))
    elif request.user.admin_user.status == "13":
        region = Region.objects.filter(Q(region='Namangan'))
    elif request.user.admin_user.status == "14":
        region = Region.objects.filter(Q(region="Farg'ona"))

    now = datetime.datetime.now()
    one_month = now + datetime.timedelta(30)
    certifs = Certificate.objects.filter(given_region__in=region, limit_date__gte=now, limit_date__lte=one_month)
    # output = ','.join(i.given_date for i in certifs)
    # return HttpResponse(output)
    return render(request, 'month_certif.html', {"certifs": certifs})


@login_required
def home_page(request):
    return render(request, 'home.html')


@login_required
def staff_page(request):

    staff_count = Staff.objects.count()
    search = request.GET.get('search')
    if search:
        search_staff = Staff.objects.filter(
            Q(first_name__icontains=search) | Q(last_name__icontains=search)
        )
        staffs = search_staff
    else:
        staffs = Staff.objects.all()
    if len(staffs) >= 7:
        paginator = Paginator(staffs, 6)
        page = request.GET.get('page')
        staffs = paginator.get_page(page)
    return render(request, 'staff.html', {'staffs': staffs, 'staff_count': staff_count})


@login_required
def upload_staff(request):
    ranks = Rank.objects.all()
    if request.method == 'POST':
        upload = StaffForm(request.POST, request.FILES)
        if upload.is_valid():
            if "add_text" in request.POST:
                upload.save()
                return redirect('add_staff')
            else:
                upload.save()
                return redirect("staff")
        else:
            upload.save()
            return redirect("staff")
        # else:
        #     print("dsfsdf", upload.errors)
        #     return HttpResponse("""your form is wrong, reload on <a href = "{{ url 'index' }}">reload</a>""")
    else:
        return render(request, 'staff_add.html', {'ranks': ranks})


@login_required
def edit_staff(request, id):
    ranks = Rank.objects.all()
    id = str(id)
    try:
        staff = Staff.objects.get(id=id)
    except Staff.DoesNotExist:
        return redirect('staff')
    staff_form = StaffForm(request.POST or None, instance=staff,files=request.FILES)
    if staff_form.is_valid():
        staff_form.save()
        return redirect('staff')
    else:
        print('afdf', staff_form.errors)
    return render(request, 'staff_edit.html', {'staff': staff, 'ranks': ranks})


@login_required
def delete_staff(request, id):
    id = int(id)
    try:
        product = Staff.objects.get(id=id)
    except Staff.DoesNotExist:
        return redirect('staff')
    product.delete()
    return redirect('staff')


@login_required
def rank_page(request):
    rank_count = Rank.objects.count()
    search = request.GET.get('search')
    if search:
        search_rank = Rank.objects.filter(
            Q(rank__icontains=search)
        )
        ranks = search_rank
    else:
        ranks = Rank.objects.all()
    if len(ranks) >= 10:
        paginator = Paginator(ranks, 9)
        page = request.GET.get('page')
        ranks = paginator.get_page(page)
    return render(request, 'rank.html', {'ranks': ranks, 'rank_count': rank_count})


@login_required
def upload_rank(request):
    upload = RankForm()
    ranks = Rank.objects.all()
    if request.method == 'POST':
        upload = RankForm(request.POST, request.FILES)
        print("dsdsds", request.POST, request.FILES)
        if upload.is_valid():
            print(True)
            if "add_text" in request.POST:
                upload.save()
                return redirect('add_rank')
            else:
                upload.save()
                return redirect("rank")
        else:
            print("errrrorrr", upload.errors)
            return HttpResponse('xatolik')
        # else:
        #     print(upload.errors)
        #     return HttpResponse("""your form is wrong, reload on <a href = "{% url 'product_page' %}">reload</a>""")
    else:
        return render(request, 'rank_add.html')



@login_required
def edit_rank(request, id):
    id = str(id)
    try:
        rank = Rank.objects.get(id=id)
    except Rank.DoesNotExist:
        return redirect('rank')
    product_form = RankForm(request.POST or None, instance=rank)
    if product_form.is_valid():
        product_form.save()
        return redirect('rank')
    return render(request, 'rank_edit.html', {'rank': rank})


@login_required
def delete_rank(request, id):
    id = int(id)
    try:
        product = Rank.objects.get(id=id)
    except Rank.DoesNotExist:
        return redirect('rank')
    product.delete()
    return redirect('rank')


@login_required
def region_page(request):
    search = request.GET.get('search')
    if search:
        search_categ = Region.objects.filter(
            Q(region__icontains=search)
        )
        regions = search_categ
    else:
        regions = Region.objects.all()
    if len(regions) >= 10:
        paginator = Paginator(regions, 9)
        page = request.GET.get('page')
        regions = paginator.get_page(page)
    return render(request, 'region.html', {'regions': regions})


@login_required
def upload_region(request):
    regions = Region.objects.all()
    if request.method == 'POST':
        upload = RegionForm(request.POST, request.FILES)
        if upload.is_valid():
            if "add_text" in request.POST:
                upload.save()
                return redirect('add_region')
            else:
                upload.save()
                return redirect("region")
        # else:
        #     print(upload.errors)
        #     return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'region_add.html', {'regions': regions})


@login_required
def edit_region(request, id):
    regions = Region.objects.all()
    id = str(id)
    try:
        region = Region.objects.get(id=id)
        # parent = region.parent
        # if parent:
        #     parent = parent
        # else:
        #     parent = '-------'
    except Region.DoesNotExist:
        return redirect('region')
    team_form = RegionForm(request.POST or None, instance=region)
    if team_form.is_valid():
        team_form.save()
        return redirect('region')
    return render(request, 'region_edit.html', {'region': region, 'regions': region})


@login_required
def delete_region(request, id):
    id = int(id)
    try:
        product = Region.objects.get(id=id)
    except Region.DoesNotExist:
        return redirect('region')
    product.delete()
    return redirect('region')


@login_required
def certif_page(request):
    if request.user.admin_user.status == '1':
        region = Region.objects.filter(Q(region='Toshkent'))
    elif request.user.admin_user.status == "2":
        region = Region.objects.filter(Q(region='Toshkent viloyati'))
    elif request.user.admin_user.status == "3":
        region = Region.objects.filter(Q(region='Sirdaryo'))
    elif request.user.admin_user.status == "4":
        region = Region.objects.filter(Q(region='Jizzax'))
    elif request.user.admin_user.status == "5":
        region = Region.objects.filter(Q(region='Samarqand'))
    elif request.user.admin_user.status == "6":
        region = Region.objects.filter(Q(region='Qashqadaryo'))
    elif request.user.admin_user.status == "7":
        region = Region.objects.filter(Q(region='Surxondaryo'))
    elif request.user.admin_user.status == "8":
        region = Region.objects.filter(Q(region='Buxoro'))
    elif request.user.admin_user.status == "9":
        region = Region.objects.filter(Q(region='Navoiy'))
    elif request.user.admin_user.status == "10":
        region = Region.objects.filter(Q(region='Xorazm'))
    elif request.user.admin_user.status == "11":
        region = Region.objects.filter(Q(region='Qoraqalpoqiston'))
    elif request.user.admin_user.status == '12':
        region = Region.objects.filter(Q(region='Andijon'))
    elif request.user.admin_user.status == "13":
        region = Region.objects.filter(Q(region='Namangan'))
    elif request.user.admin_user.status == "14":
        region = Region.objects.filter(Q(region="Farg'ona"))

    certif_count = Certificate.objects.count()
    print(certif_count)
    print(request.GET)
    search = request.GET.get('search')
    print(search)
    if search:
        search_categ = Certificate.objects.filter(
            Q(staff__icontains=search)
        )
        certificates = search_categ
    else:
        certificates = Certificate.objects.filter(given_region__in=region)

        # certificates = Certificate.objects.all()
    if len(certificates) >= 10:
        paginator = Paginator(certificates, 9)
        page = request.GET.get('page')
        certificates = paginator.get_page(page)
    return render(request, 'certif.html', {'certificates': certificates, 'certif_count': certif_count,})


@login_required
def upload_certif(request):
    if request.user.admin_user.status == '1':
        region = Region.objects.filter(Q(region='Toshkent'))
    elif request.user.admin_user.status == "2":
        region = Region.objects.filter(Q(region='Toshkent viloyati'))
    elif request.user.admin_user.status == "3":
        region = Region.objects.filter(Q(region='Sirdaryo'))
    elif request.user.admin_user.status == "4":
        region = Region.objects.filter(Q(region='Jizzax'))
    elif request.user.admin_user.status == "5":
        region = Region.objects.filter(Q(region='Samarqand'))
    elif request.user.admin_user.status == "6":
        region = Region.objects.filter(Q(region='Qashqadaryo'))
    elif request.user.admin_user.status == "7":
        region = Region.objects.filter(Q(region='Surxondaryo'))
    elif request.user.admin_user.status == "8":
        region = Region.objects.filter(Q(region='Buxoro'))
    elif request.user.admin_user.status == "9":
        region = Region.objects.filter(Q(region='Navoiy'))
    elif request.user.admin_user.status == "10":
        region = Region.objects.filter(Q(region='Xorazm'))
    elif request.user.admin_user.status == "11":
        region = Region.objects.filter(Q(region='Qoraqalpoqiston'))
    elif request.user.admin_user.status == '12':
        region = Region.objects.filter(Q(region='Andijon'))
    elif request.user.admin_user.status == "13":
        region = Region.objects.filter(Q(region='Namangan'))
    elif request.user.admin_user.status == "14":
        region = Region.objects.filter(Q(region="Farg'ona"))
    staffs = Staff.objects.all()
    if request.method == 'POST':
        upload = CertificateForm(request.POST, request.FILES)
        if upload.is_valid():
            if "add_text" in request.POST:
                upload.save()
                return redirect('add_certif')
            else:
                upload.save()
                return redirect("certif")
    else:
        # return HttpResponse('sadf')
        return render(request,'certif_add.html',{'staffs': staffs,'admin_region': region})


    #         return render(request,'certif_add.html',{'staffs': staffs, 'regions': regions, 'admin_region': region})
    #         print("errrrorrr", upload.errors)
    #         return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    # else:
    #     return render(request, 'certif_add.html', {'staffs': staffs, 'regions': regions, 'admin_region': region})


@login_required
def edit_sertif(request, id):
    print(id)
    id = str(id)
    staffs = Staff.objects.all()

    if request.user.admin_user.status == '1':
        region = Region.objects.filter(Q(region='Toshkent'))
    elif request.user.admin_user.status == "2":
        region = Region.objects.filter(Q(region='Toshkent viloyati'))
    elif request.user.admin_user.status == "3":
        region = Region.objects.filter(Q(region='Sirdaryo'))
    elif request.user.admin_user.status == "4":
        region = Region.objects.filter(Q(region='Jizzax'))
    elif request.user.admin_user.status == "5":
        region = Region.objects.filter(Q(region='Samarqand'))
    elif request.user.admin_user.status == "6":
        region = Region.objects.filter(Q(region='Qashqadaryo'))
    elif request.user.admin_user.status == "7":
        region = Region.objects.filter(Q(region='Surxondaryo'))
    elif request.user.admin_user.status == "8":
        region = Region.objects.filter(Q(region='Buxoro'))
    elif request.user.admin_user.status == "9":
        region = Region.objects.filter(Q(region='Navoiy'))
    elif request.user.admin_user.status == "10":
        region = Region.objects.filter(Q(region='Xorazm'))
    elif request.user.admin_user.status == "11":
        region = Region.objects.filter(Q(region='Qoraqalpoqiston'))
    elif request.user.admin_user.status == '12':
        region = Region.objects.filter(Q(region='Andijon'))
    elif request.user.admin_user.status == "13":
        region = Region.objects.filter(Q(region='Namangan'))
    elif request.user.admin_user.status == "14":
        region = Region.objects.filter(Q(region="Farg'ona"))

    try:
        sertif = Certificate.objects.get(id=id)
        value = sertif.given_date
        d = value.strftime("%d.%m.%Y")
        print(d)
        print("p",str(d)[-1])
    except Certificate.DoesNotExist:
        return redirect('certif')
    if request.method == "POST":
        custom_form = CertificateForm(request.POST,instance=sertif)
        if custom_form.is_valid():
            custom_form.save()
            return redirect('certif')
    else:
        return render(request, 'certif_edit.html', {'sertif': sertif, 'staffs': staffs, 'regions': region, 't': d})


@login_required
def delete_sertif(request, id):
    id = int(id)
    try:
        product = Certificate.objects.get(id=id)
    except Certificate.DoesNotExist:
        return redirect('certif')
    product.delete()
    return redirect('certif')


@login_required
def unit_page(request):
    search = request.GET.get('search')
    if search:
        search_categ = Unit.objects.filter(
            Q(name__icontains=search)
        )
        units = search_categ
    else:
        units = Unit.objects.all()
    return render(request, 'unit.html', {'units': units})


@login_required
def upload_unit(request):
    upload = UnitForm()
    if request.method == 'POST':
        upload = UnitForm(request.POST, request.FILES)
        if upload.is_valid():
            if "add_text" in request.POST:
                upload.save()
                return redirect('upload_unit')
            else:
                upload.save()
                return redirect("unit_page")
        # else:
        #     return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'unit_add.html')


@login_required
def edit_unit(request, id):
    id = str(id)
    try:
        unit = Unit.objects.get(id=id)
    except Unit.DoesNotExist:
        return redirect('unit_page')
    unit_form = UnitForm(request.POST or None, instance=unit)
    if unit_form.is_valid():
        unit_form.save()
        return redirect('unit_page')
    return render(request, 'unit_edit.html', {'unit': unit})


@login_required
def delete_unit(request, id):
    id = int(id)
    try:
        product = Unit.objects.get(id=id)
    except Unit.DoesNotExist:
        return redirect('unit_page')
    product.delete()
    return redirect('unit_page')

