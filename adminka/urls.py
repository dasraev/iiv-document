from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path("month/", views.month_certif, name='month_certif'),
    path("end_certif/", views.end_certif, name='end_certif'),
    path('index/', views.home_page, name='index'),
    path('staff_page/', views.staff_page, name='staff'),
    path('staff_add/', views.upload_staff, name='add_staff'),
    path('staff_edit/<int:id>', views.edit_staff, name='edit_staff'),
    path('delete_staff/<int:id>', views.delete_staff, name='delete_staff'),
    path('rank_page/', views.rank_page, name='rank'),
    path('rank_upload/', views.upload_rank, name='add_rank'),
    path('edit_rank/<int:id>', views.edit_rank, name='edit_rank'),
    path('delete_rank/<int:id>', views.delete_rank, name='delete_rank'),
    path('unit_page/', views.unit_page, name='unit_page'),
    path('upload_unit/', views.upload_unit, name='upload_unit'),
    path('edit_unit/<int:id>', views.edit_unit, name='edit_unit'),
    path('delete_unit/<int:id>', views.delete_unit, name='delete_unit'),
    path('sertif_page/', views.certif_page, name='certif'),
    path('upload_certif/', views.upload_certif, name='add_certif'),
    path('edit_certif/<int:id>', views.edit_sertif, name='edit_certif'),
    path('delete_certif/<int:id>', views.delete_sertif, name='delete_certif'),
    path('region_page/', views.region_page, name='region'),
    path('region_upload/', views.upload_region, name='add_region'),
    path('edit_region/<int:id>', views.edit_region, name='edit_region'),
    path('delete_region/<int:id>', views.delete_region, name='delete_region'),

]