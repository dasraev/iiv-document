from django.db import models
from django.contrib.auth.models import User, AbstractBaseUser


class AdminUser(models.Model):
    Choice = (
        ("1", "Toshkent shahar"),
        ("2", "Toshkent viloyati"),
        ("3", "Sirdaryo"),
        ("4", "Jizzax"),
        ("5", "Samarqand"),
        ("6", "Qashqadaryo"),
        ("7", "Surxondaryo"),
        ("8", "Buxoro"),
        ("9", "Navoiy"),
        ("10", "Xorazm"),
        ("11", "Qoraqalpoqiston"),
        ("12", "Andijon"),
        ("13", "Namangan"),
        ("14", "Farg'ona"),
    )
    user = models.OneToOneField(User, blank=True, null=True, related_name="admin_user", on_delete=models.DO_NOTHING)
    status = models.CharField(max_length=2, blank=True, null=True, choices=Choice)

    def __str__(self):
        return self.user.username
