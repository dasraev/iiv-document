from django import forms
from staff.models import Unit, Region, Staff, Certificate, Rank


class RegionForm(forms.ModelForm):

    class Meta:
        model = Region
        fields = '__all__'


class StaffForm(forms.ModelForm):

    class Meta:
        model = Staff
        fields = '__all__'


class CertificateForm(forms.ModelForm):

    class Meta:
        model = Certificate
        fields = '__all__'


class RankForm(forms.ModelForm):

    class Meta:
        model = Rank
        fields = '__all__'


class UnitForm(forms.ModelForm):

    class Meta:
        model = Unit
        fields = '__all__'
